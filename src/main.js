import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import axios from 'axios'
import VueAxios from 'vue-axios'
// import moment from 'moment'

Vue.use(VueAxios, axios)
Vue.config.productionTip = false
Vue.prototype.$base_url = '../api';
// Vue.prototype.$base_url = 'https://vision.pienergy-analytics.com/grid-pi-dev/public/api';
// Vue.prototype.$base_url = 'http://localhost/grid-pi-dev/public/api';
Vue.prototype.$axios = axios;

new Vue({
  router,
  store,
  vuetify,
  render: function (h) { return h(App) }
}).$mount('#app')
