import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Configuration from '../views/config/Configuration.vue';
import Login from '../views/Login.vue';
import TagConfiguration from '../views/config/TagConfiguration.vue';
import APITester from '../views/config/APITester.vue';
import MapDataLogger from '../views/config/MapDataLogger.vue';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect : {name : 'Configuration'}
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: function () {
      return import(/* webpackChunkName: "about" */ '../views/About.vue')
    }
  },
  {
    path : '/login',
    name : 'Login',
    component : Login,
    beforeEnter : (to, from, next) => {
      if(localStorage.getItem('token')) {
        next({name : 'Configuration'});
      }
      else {
        next()
      }
    }
  },
  {
    path : '/configurations',
    name : 'Configuration',
    component : Configuration,
    beforeEnter : (to, from, next) => {
      if(localStorage.getItem('token')) {
        next();
      }
      else {
        next({name : 'Login'});
      }
    }
  },
  {
    path : '/tag-configurations',
    name : 'TagConfiguration',
    component : TagConfiguration,
    beforeEnter : (to, from, next) => {
      if(localStorage.getItem('token')) {
        next();
      }
      else {
        next({name : 'Login'});
      }
    }
  },
  {
    path : '/api-tester',
    name : 'APITester',
    component : APITester,
    beforeEnter : (to, from, next) => {
      if(localStorage.getItem('token')) {
        next();
      }
      else {
        next({name : 'Login'});
      }
    }
  },
  {
    path : '/map-data-logger',
    name : 'MapDataLogger',
    component : MapDataLogger,
    beforeEnter : (to, from, next) => {
      if(localStorage.getItem('token')) {
        next();
      }
      else {
        next({name : 'Login'});
      }
    }
  },
]

const router = new VueRouter({
  routes
})

export default router
